#!/bin/bash

cat vendor/camera/mimoji/model2.zip.* 2>/dev/null >> vendor/camera/mimoji/model2.zip
rm -f vendor/camera/mimoji/model2.zip.* 2>/dev/null
cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat vendor/app/MiGameService/MiGameService.apk.* 2>/dev/null >> vendor/app/MiGameService/MiGameService.apk
rm -f vendor/app/MiGameService/MiGameService.apk.* 2>/dev/null
cat vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> vendor/data-app/SmartHome/SmartHome.apk
rm -f vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat system/system/lib64/librelight_only_extraphoto.so.* 2>/dev/null >> system/system/lib64/librelight_only_extraphoto.so
rm -f system/system/lib64/librelight_only_extraphoto.so.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/app/Music/Music.apk.* 2>/dev/null >> system/system/app/Music/Music.apk
rm -f system/system/app/Music/Music.apk.* 2>/dev/null
cat system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> system/system/priv-app/MiuiCamera/MiuiCamera.apk
rm -f system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat system/system/priv-app/MiuiVideo/MiuiVideo.apk.* 2>/dev/null >> system/system/priv-app/MiuiVideo/MiuiVideo.apk
rm -f system/system/priv-app/MiuiVideo/MiuiVideo.apk.* 2>/dev/null
cat system/system/priv-app/Browser/Browser.apk.* 2>/dev/null >> system/system/priv-app/Browser/Browser.apk
rm -f system/system/priv-app/Browser/Browser.apk.* 2>/dev/null
cat system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk.* 2>/dev/null >> system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk
rm -f system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk.* 2>/dev/null
cat system/system/data-app/com.ss.android.ugc.aweme_15/com.ss.android.ugc.aweme_15.apk.* 2>/dev/null >> system/system/data-app/com.ss.android.ugc.aweme_15/com.ss.android.ugc.aweme_15.apk
rm -f system/system/data-app/com.ss.android.ugc.aweme_15/com.ss.android.ugc.aweme_15.apk.* 2>/dev/null
cat system/system/data-app/Youpin/Youpin.apk.* 2>/dev/null >> system/system/data-app/Youpin/Youpin.apk
rm -f system/system/data-app/Youpin/Youpin.apk.* 2>/dev/null
cat system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk.* 2>/dev/null >> system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk
rm -f system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk.* 2>/dev/null
cat system/system/data-app/MiShop/MiShop.apk.* 2>/dev/null >> system/system/data-app/MiShop/MiShop.apk
rm -f system/system/data-app/MiShop/MiShop.apk.* 2>/dev/null
cat vendor_bootimg/09_dtbdump_'z#raPk*.dtb.* 2>/dev/null >> vendor_bootimg/09_dtbdump_'z#raPk*.dtb
rm -f vendor_bootimg/09_dtbdump_'z#raPk*.dtb.* 2>/dev/null
cat bootimg/01_dtbdump_.* 2>/dev/null >> bootimg/01_dtbdump_
rm -f bootimg/01_dtbdump_.* 2>/dev/null
cat L8Mww.dtb.* 2>/dev/null >> L8Mww.dtb
rm -f L8Mww.dtb.* 2>/dev/null
cat cust/app/customized/partner-com.eg.android.AlipayGphone_23/partner-com.eg.android.AlipayGphone_23.apk.* 2>/dev/null >> cust/app/customized/partner-com.eg.android.AlipayGphone_23/partner-com.eg.android.AlipayGphone_23.apk
rm -f cust/app/customized/partner-com.eg.android.AlipayGphone_23/partner-com.eg.android.AlipayGphone_23.apk.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/priv-app/MiuiSystemUI/MiuiSystemUI.apk.* 2>/dev/null >> system_ext/priv-app/MiuiSystemUI/MiuiSystemUI.apk
rm -f system_ext/priv-app/MiuiSystemUI/MiuiSystemUI.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
