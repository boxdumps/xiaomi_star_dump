## missi-user 11 RKQ1.201112.002 V12.5.12.0.RKACNXM release-keys
- Manufacturer: xiaomi
- Platform: lahaina
- Codename: star
- Brand: Xiaomi
- Flavor: missi-user
- Release Version: 11
- Id: RKQ1.201112.002
- Incremental: V12.5.12.0.RKACNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: 560
- Fingerprint: Xiaomi/star/star:11/RKQ1.201112.002/V12.5.12.0.RKACNXM:user/release-keys
- OTA version: 
- Branch: missi-user-11-RKQ1.201112.002-V12.5.12.0.RKACNXM-release-keys
- Repo: xiaomi_star_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
